#!/bin/bash

###########################################################################################
# Configuration parameters
###########################################################################################

TYPE="nano"                         	# Communication pattern: nano/mqtt
LOG_FILES_PATH="./benchmarks/tempdir"      # Directory where results should be stored
NUMBER_OF_LOOPS=1                   	# Number of loops that should be executed
REALTIME=false                           # Define if RT or NRT Simulation should be performed

DISTAIX_PATH="distaix"              	# Path to distaix main dir
DISTAIX_HOST="swarm0"			        # Name of host where distaix will be executed
DIST_NUM_OF_PROCESSES=20            	    # Number of processes invoked by distaix

DPSIM_PATH="dpsim-applications/dpsim"   # Path to dpsim main dir
DOCKER_CONTAINER_NAME="bench_cont"  	# Name of dpsim_dev docker container

###########################################################################################
###########################################################################################

set -x

_kill(){
	echo "Caught kill signal!"
	docker kill bench_cont
	docker rm bench_cont
	echo "Killed and removed docker container!.."
	kill -KILL $$
}

trap _kill 2 INT SIGINT

#
# Clean up remaining docker containers if previous run crashed or container name for some
# reason exists. Start a fresh instance afterwards!
#
docker kill bench_cont
docker rm bench_cont
docker run -t --name $DOCKER_CONTAINER_NAME -d --network=host -v $(pwd)/dpsim-applications:/dpsimapp --privileged rwthacs/dpsim-dev bash

docker exec -w /dpsimapp/dpsim $DOCKER_CONTAINER_NAME rm -rf logs/Shmem_WSCC-9bus_cosim_benchmark

sleep 2	# Make sure the container is successfully created


#
# Parse optional script arguments
#
if [ "$#" -eq 0 ]; then
    echo "No parameters given -> Default parameters are used..."
    echo "Usage: ./start_benchmark.sh <numberOfLoops=1> <dockerContainerName=bench_cont>" 
elif [ "$#" -eq 1 ]; then 
    NUMBER_OF_LOOPS=$1
else
    NUMBER_OF_LOOPS=$1
    DOCKER_CONTAINER_NAME=$2
fi

#
# Set Realtime flag
#

if [ "$REALTIME" = true ]; then
    RT="-r"
else
    RT=""
fi
#
# Set paths to executables...
#
if [ "$DISTAIX_HOST" != "" ]; then	# If DistAIX should be started on remote host...
	echo "Different host for DistAIX set to: $DISTAIX_HOST ..."

	if [ "$DIST_NUM_OF_PROCESSES" -ge 24 ]; then	
		DISTAIX_EXEC="ssh wege@$DISTAIX_HOST tmux send-keys -t cosim $(pwd)/$DISTAIX_PATH/bin/run_cluster.sh\ -n$DIST_NUM_OF_PROCESSES\ -h 0,1 Enter" 	
	elif [ "$DIST_NUM_OF_PROCESSES" -ge 48 ]; then
		DISTAIX_EXEC="ssh wege@$DISTAIX_HOST tmux send-keys -t cosim $(pwd)/$DISTAIX_PATH/bin/run_cluster.sh\ -n$DIST_NUM_OF_PROCESSES\ -h 0,1,2 Enter" 	
	else
		DISTAIX_EXEC="ssh wege@$DISTAIX_HOST tmux send-keys -t cosim $(pwd)/$DISTAIX_PATH/bin/run.sh\ -n$DIST_NUM_OF_PROCESSES Enter"
	fi
else	# Else start at current host as well
	echo "Execute DistAIX simulation on same host..."
	
	if [ "$DIST_NUM_OF_PROCESSES" -ge 24 ]; then	
		DISTAIX_EXEC="$DISTAIX_PATH/bin/run_cluster.sh -n$DIST_NUM_OF_PROCESSES -h 0,1
"	
	elif [ "$DIST_NUM_OF_PROCESSES" -ge 48 ]; then
		DISTAIX_EXEC="$DISTAIX_PATH/bin/run_cluster.sh -n$DIST_NUM_OF_PROCESSES -h 0,1,2" 	
	else
		DISTAIX_EXEC="$DISTAIX_PATH/bin/run.sh -n$DIST_NUM_OF_PROCESSES"
	fi

fi
# Start DPsim in created docker container
DPSIM_EXEC="docker exec -w /dpsimapp $DOCKER_CONTAINER_NAME ./scripts/shmem_distaix_cosim/start_Shmem_cosim_benchmark_$TYPE.sh $RT"

echo "$DPSIM_EXEC"

#
# Print used configuration
#
echo "###############################################################"
echo "###############################################################"
echo "Configuration:"
echo -e "\tNumber of loops: \t\t$NUMBER_OF_LOOPS"
echo -e "\tType: \t\t\t\t$TYPE"
echo -e "\tLog files path: \t\t$LOG_FILES_PATH"
echo -e ""
echo -e "\tDistAIX path: \t\t\t$DISTAIX_PATH"
echo -e "\tNumber of DistAIX processes: \t$DIST_NUM_OF_PROCESSES"
echo -e "\tDistAIX Host: \t\t\t$DISTAIX_HOST"
echo -e ""
echo -e "\tDPsim path: \t\t\t$DPSIM_PATH"
echo -e "\tDocker container: \t\t$DOCKER_CONTAINER_NAME"
echo "###############################################################"
echo "###############################################################"

#sleep 5

# Create folder for results of current session
# remove whitespaces that are somehow introduced by 'date'...
LOG_FILES_PATH_SESSION="$(echo -e $LOG_FILES_PATH/$(date +'%d-%m-%y_%r')_$TYPE | tr -d '[:space:]')"
mkdir -v -p "$LOG_FILES_PATH_SESSION"
touch "$LOG_FILES_PATH_SESSION/summary.csv"


#
# Start cosimulation...
#
for ((i=1; i<=$NUMBER_OF_LOOPS; i++)); do
    # Execute simulations
    
    $DISTAIX_EXEC& #> /dev/null
    #sleep 5
    $DPSIM_EXEC #> /dev/null

    # Wait for everything to terminate...
    sleep 1

    # Save results of both simulations
    cp $DPSIM_PATH/logs/Shmem_WSCC-9bus_cosim_benchmark/Shmem_WSCC-9bus_cosim_benchmark.log $LOG_FILES_PATH_SESSION/${i}_dpsim.log
    cp $DPSIM_PATH/logs/Shmem_WSCC-9bus_cosim_benchmark/Shmem_WSCC-9bus_cosim_benchmark.csv $LOG_FILES_PATH_SESSION/${i}_results.csv
    cp $DISTAIX_PATH/bin/results/rank0.csv $LOG_FILES_PATH_SESSION/${i}_time.csv
    cp $DISTAIX_PATH/bin/runlog/rank0.log $LOG_FILES_PATH_SESSION/${i}_log.log
    #cp $DISTAIX_PATH/bin/results/agents/agent_1.csv $LOG_FILES_PATH_SESSION/${i}_distaix.csv

    # Parse simulation time from dpsim log file and add it to summary.csv
    VAL=$(grep -n 'Simulation' $LOG_FILES_PATH_SESSION/${i}_dpsim.log | awk -F ': ' '{print $2}')
    echo -e "$i,$VAL" >> "$LOG_FILES_PATH_SESSION/summary.csv"
    
done

#
# Clean up docker container
#
docker kill $DOCKER_CONTAINER_NAME
docker rm $DOCKER_CONTAINER_NAME
