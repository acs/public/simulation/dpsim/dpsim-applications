TOP=${TOP:-$(git rev-parse --show-toplevel)}
PATH=${TOP}/build

DURATION=5

# ms range
for i in {1..20..1}
do
    TIMESTEP=${i}e-3
    EMT_CIGRE_MV_withDG_withLoadStep --timestep=${TIMESTEP} --duration=${DURATION} ${TOP}/dpsim/Examples/CIM/grid-data/CIGRE_MV/NEPLAN/CIGRE_MV_no_tapchanger_noLoad1_LeftFeeder_With_LoadFlow_Results/*.xml
    DP_CIGRE_MV_withDG_withLoadStep --timestep=${TIMESTEP} --duration=${DURATION} ${TOP}/dpsim/Examples/CIM/grid-data/CIGRE_MV/NEPLAN/CIGRE_MV_no_tapchanger_noLoad1_LeftFeeder_With_LoadFlow_Results/*.xml
done

# us range
for i in {100..900..100}
do
    TIMESTEP=${i}e-6
    EMT_CIGRE_MV_withDG_withLoadStep --timestep=${TIMESTEP} --duration=${DURATION} ${TOP}/dpsim/Examples/CIM/grid-data/CIGRE_MV/NEPLAN/CIGRE_MV_no_tapchanger_noLoad1_LeftFeeder_With_LoadFlow_Results/*.xml
    DP_CIGRE_MV_withDG_withLoadStep --timestep=${TIMESTEP} --duration=${DURATION} ${TOP}/dpsim/Examples/CIM/grid-data/CIGRE_MV/NEPLAN/CIGRE_MV_no_tapchanger_noLoad1_LeftFeeder_With_LoadFlow_Results/*.xml
done