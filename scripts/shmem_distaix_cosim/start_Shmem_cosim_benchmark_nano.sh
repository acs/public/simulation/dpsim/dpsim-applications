#!/bin/bash

set +x

_stop() {
	echo "Caught SIGTSTP signal!"
	kill -TSTP ${CHILDS} 2>/dev/null
}

_cont() {
	echo "Caught SIGCONT signal!"
	kill -CONT ${CHILDS} 2>/dev/null
}

_term() {
	echo "Caught SIGTERM signal!"
	kill -TERM ${VN} ${CHILDS} 2>/dev/null
}

_kill() {
	echo "Caught SIGKILL signal!"
	kill -KILL ${VN} ${CHILDS} 2>/dev/null
}

trap _stop SIGTSTP
trap _cont SIGCONT
trap _term SIGTERM
trap _kill SIGKILL
trap _kill SIGINT

CHILDS=""
PIPE=false
REALTIME=false

while getopts "h?pr" opt; do
    case "$opt" in
	h|\?)
		echo "##############################################################"
		echo "Options:"
		echo -e "\t -p \t\t Use VILLASpipe instead of VILLASnode"
		echo -e "\t -r \t\t Start as RealTimeSimulation"
		echo "##############################################################"
		exit 0
		;;
    p)
        PIPE=true
        ;;
    r)  
		REALTIME=true
        ;;
    esac
done

# Start time
TIME=$(date -d "+20 seconds" +%Y%m%dT%H%M%S) #-Iseconds
echo "Start simulation at: $TIME"

if [ "$PIPE" = true ]; then
	VILLAS_LOG_PREFIX="[Pipe] " \
	villas-pipe configs/shmem_distaix_cosim/Shmem_cosim_dev_nano.conf dpsim
else
	VILLAS_LOG_PREFIX="[Node] " \
	villas-node configs/shmem_distaix_cosim/Shmem_cosim_dev_nano.conf & VN=$!
	#:
fi

# Wait until node is successfully started
sleep 5

if [ "$REALTIME" = true ]; then
	echo "Starting DPsim as RealTimeSimulation..."
	CPS_LOG_PREFIX="[SYS ]" \
	build/Shmem_WSCC-9bus_cosim_benchmark_realtime #& P1=$!
else
	CPS_LOG_PREFIX="[SYS ]" \
	build/Shmem_WSCC-9bus_cosim_benchmark #& P1=$!

fi
#CHILDS=$P1

sleep 2

pkill villas-node

# Wait until all child processed finished
# while (( $(ps --no-headers -o pid --ppid=$$ | wc -w) > 1 )); do
# 	wait
# done
